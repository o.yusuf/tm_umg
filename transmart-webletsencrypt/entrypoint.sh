#!/bin/sh

sed -i 's/server_name.*/server_name '${DOMAIN}';/' /etc/nginx/conf.d/app.conf

nginx -g "daemon off;"

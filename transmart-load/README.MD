# Build
docker build -t transmart-load .

# Show logs
docker logs -f transmart-load

# Remove container
docker rm -f transmart-load

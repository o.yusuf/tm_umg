repositories <- c("https://ftp.gwdg.de/pub/misc/cran/", "https://cloud.r-project.org/")
packs <- c("reshape", "reshape2", "ggplot2", "data.table", "Cairo", "snowfall", "gplots", "foreach", "doParallel", "visreg", "pROC", "jsonlite", "RUnit", "BiocManager");

install.packages(packs, repos=repositories, dependencies=TRUE);
bioc_packs <- c("impute","preprocessCore","GO.db","AnnotationDbi","multtest","CGHbase","edgeR","snpStats","QDNAseq");
BiocManager::install(bioc_packs);

# WCGNA (from CRAN) depends on some Bioconductor packages, hence it is installed after them
install.packages("WGCNA", repos=repositories, dependencies=TRUE);
install.packages("Rserve",,"http://rforge.net");

# Additional packages needed
packs_2 <- c("vegan", "pheatmap", "RColorBrewer", "matrixStats", "plyr", "kinship2", "gtools", "tidyr")
bioc_packs_2 <- c("viper", "limma")

install.packages(packs_2, repos=repositories, dependencies=TRUE);
BiocManager::install(bioc_packs_2);

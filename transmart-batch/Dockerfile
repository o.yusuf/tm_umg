FROM debian:jessie
MAINTAINER Christian Bauer <christian.bauer@med.uni-goettingen.de>

ENV TABLESPACES=dummy \
    PATH=/root/.sdkman/candidates/groovy/2.4.5/bin:$PATH \
    KETTLE_JOBS_PSQL=/transmart-data/env/tranSMART-ETL/Postgres/GPL-1.0/Kettle/Kettle-ETL/ \
    KITCHEN=/transmart-data/env/data-integration/kitchen.sh \
    PGHOST=tmdb \
    PGPORT=5432 \
    PGDATABASE=transmart \
    PGSQL_BIN=/usr/bin \
    PATH=/root/.sdkman/candidates/groovy/2.4.5/bin:$PATH \
    JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64

# using jessie backports archive
RUN echo "deb [check-valid-until=no] http://archive.debian.org/debian jessie-backports main" \
     > /etc/apt/sources.list.d/jessie-backports.list && \
    sed -i '/deb http:\/\/deb.debian.org\/debian jessie-updates main/d' /etc/apt/sources.list && \
    apt-get -o Acquire::Check-Valid-Until=false update

RUN apt-get -y install \
     git \
     postgresql-client \
     php5-cli \
     php5-json \
     xz-utils \
     zip \
     make \
     curl \
     ca-certificates \
     php5 \
     vim \
     wget \
     unzip \
    software-properties-common && \
    apt-get install -y -t jessie-backports \
     openjdk-8-jdk-headless \
     ca-certificates-java && \
    update-alternatives --install /usr/bin/java java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java 3000 && \
    update-alternatives --config java && \
    rm -rf /var/lib/apt/lists/* # Clean apt cache

# Installing transmart-batch
RUN  wget "https://bouncycastle.org/download/bcprov-ext-jdk15on-158.jar" -O /usr/lib/jvm/java-8-openjdk-amd64/jre/lib/ext/bcprov-ext-jdk15on-158.jar && \
  perl -pi.bak -e 's/^(security\.provider\.)([0-9]+)/$1.($2+1)/ge' /etc/java-8-openjdk/security/java.security && \
  echo "security.provider.1=org.bouncycastle.jce.provider.BouncyCastleProvider" | tee -a /etc/java-8-openjdk/security/java.security
WORKDIR /opt/git/
RUN git clone https://github.com/tranSMART-Foundation/transmart-batch.git --branch release-16.2 transmart-batch
WORKDIR /opt/git/transmart-batch
RUN echo org.gradle.java.home=/usr/lib/jvm/java-8-openjdk-amd64 >> gradle.properties && \
# does not work, TODO: make default java Xmx bigger    echo org.gradle.jvmargs=-Xmx4GB >> gradle.properties && \
    ./gradlew capsule
RUN mv build/libs/* ./transmart-batch.jar 

# run transmart-batch once
WORKDIR /opt/git/transmart-batch/
RUN ./transmart-batch.jar; echo ""
RUN alias tmbatch='/opt/git/transmart-batch/transmart-batch.jar -c /opt/git/transmart-batch/batchdb.properties' >> ~/.bashrc

# install MonitorDirectory
WORKDIR /opt/git/
RUN git clone https://gitlab.gwdg.de/medinfpub/tos/MonitorDirectory.git

LABEL TRANSMART_VERSION="TMF16.2"

# add local example studie
ADD GSE7390.zip /opt/

COPY entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]

#!/bin/bash

echo "Updating database connection file to current password from .env file."

# create psql connect file
file="/root/.pgpass"
if [ -f "$file" ] ; then
  rm $file
fi

touch $file
echo "*:*:*:postgres:${POSTGRES_PASSWORD}" >> $file
chmod 0600 $file

# wait until database is available with current password
until psql -h "$PGHOST" -d "postgres" -U "postgres" -w -c '\l'; do
  >&2 echo "Postgres is unavailable with current password - sleeping ..."
  sleep 1
done

>&2 echo "Postgres is accessible with current password. Setting up transmart-batch database configuration."


# create batch properties file
file="/opt/git/transmart-batch/batchdb.properties"
if [ -f "$file" ] ; then
  rm $file
fi

touch $file
echo "batch.jdbc.driver=org.postgresql.Driver" >> $file
echo "batch.jdbc.url=jdbc:postgresql://${PGHOST}:${PGPORT}/${PGDATABASE}" >> $file
echo "batch.jdbc.user=postgres" >> $file
echo "batch.jdbc.password=${POSTGRES_PASSWORD}" >> $file

# setup transmart-batch schema
psql -U postgres -c "\dn transmart.*" -w | grep -q ts_batch

if [ $? -eq 0 ]; then
 echo "No modification of the database: transmart-batch tables already exist."
else
 echo "The transmart-batch tables do not exist. Setting up the schema now."
 cd /opt/git/transmart-batch/
 ./gradlew setupSchema
 cd /opt
fi

# install and run MonitorDirectory
if pgrep -f "MonitorDirectory" > /dev/null
then
 echo MonitorDirectory is already running.
else
 echo Prepare and run MonitorDirectory.
 cd /opt/git/MonitorDirectory/MonitorDirectory/
 chmod +x MonitorDirectory_run.sh
 mkdir -p /opt/data/logs/
 mkdir /opt/data/incoming/
 ./MonitorDirectory_run.sh >> /opt/data/logs/MonitorDirectory.log &
fi

# upload example study
ls /opt/data/archive | grep -q .
if [ $? -eq 0 ]; then
 echo ""
else
 echo "Archive folder seems to be empty. Uploading example study data ..."
 cp /opt/GSE7390.zip /opt/data/incoming/
fi

# send log to console
tail -f /opt/data/logs/MonitorDirectory.log

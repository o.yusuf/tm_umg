#!/bin/bash

echo "Changing postgres system user password to current one defined in .env file."

# change postgres user password to POSTGRES_PASSWORD
echo -e "$POSTGRES_PASSWORD\n$POSTGRES_PASSWORD" | passwd postgres

#echo "$POSTGRES_PASSWORD\n$POSTGRES_PASSWORD"

# change the passwords in the database (utilizing the variable "sql" to escape some escaping problems)
echo "Changing database passwords to current one defined in .env file."

su postgres -c 'etc/init.d/postgresql start'
sleep 5
sudo -u postgres psql -U postgres -d transmart -f /usr/bin/optimize_database.sql
sql="ALTER USER postgres WITH PASSWORD '$POSTGRES_PASSWORD';"
sudo -u postgres psql -U postgres -c "$sql"
sql="ALTER USER biomart_user WITH PASSWORD '$POSTGRES_PASSWORD';"
sudo -u postgres psql -U postgres -c "$sql"
su postgres -c '/etc/init.d/postgresql stop'

# start db
su postgres -c '/usr/lib/postgresql/9.3/bin/postgres -D /var/lib/postgresql/9.3/main -c config_file=/etc/postgresql/9.3/main/postgresql.conf'
